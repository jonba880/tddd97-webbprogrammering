__author__ = 'Hugo Moritz and Jonatan Barr'
import sqlite3
import json
import hashlib
from bcrypt import hashpw, gensalt
from flask import Flask
from flask import jsonify
from flask import g

DATABASE = 'database.db'

def connect_db():
    g.db = sqlite3.connect(DATABASE)

def close_db():
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

#For sign_in purpose, return email and password if exisitng in database
def email_password(email, password):
    try:
        result = []
        salt = get_salt_by_email(email)
        password = hashpw(str(password), str(salt))
        cursor = g.db.execute("select count(*) from user where email = ? and password = ? LIMIT 1", [email, password])
        returnvalue = cursor.fetchone()[0]
        cursor.close()
        if returnvalue == 1:
            return True;
        return False
    except:
        return False

#Saves token for correct email and password
def save_token(email, password, token):
    try:
        salt = get_salt_by_email(email)
        password = hashpw(str(password), str(salt))
        cursor = g.db.execute("UPDATE user set token = ? where email = ? and password = ?", [token, email, password])
        g.db.commit()
        cursor.close()
        return True
    except:
        return False

#Regiser a user, with all information. Returns false if not possible to register in database.
def register(email, password, firstname, familyname, gender, city, country):
	try:
		token = ""
		salt = ""
		salt = gensalt()
		password = hashpw(str(password), str(salt))
		cursor = g.db.execute("insert into user values(?,?,?,?,?,?,?,?,?)", [email, password, firstname, familyname, gender, city, country, token, salt])
		g.db.commit()
		cursor.close()
		return True
	except:
		return False

#Get token.
def get_token(token):
    try:
        cursor = g.db.execute("select token from user where token = ?", [token])
        auth = cursor.fetchone()[0]
        cursor.close()
        if auth == token:
            return True
        return False
    except:
        return False

#Get token by email
def get_token_by_email(email):
    cursor = g.db.execute("select token from user where email = ?", [email])
    token = cursor.fetchone()[0]
    cursor.close()
    return token

#Get email by token
def get_email(token):
    cursor = g.db.execute("select email from user where token = ?", [token])
    email = cursor.fetchone()[0]
    cursor.close()
    return email

#Delete token
def delete_token(email):
    try:
        cursor = g.db.execute("UPDATE user set token = NULL where email = ?", [email])
        g.db.commit()
        cursor.close()
        return True
    except:
        return False

#Set new password
def set_password(token, newPassword):
    try:
        salt = get_salt_by_token(token)
        #print("set new pass salt:")
        #print(salt)
        newPassword = hashpw(str(newPassword), str(salt))
        cursor = g.db.execute("UPDATE user SET password = ? WHERE token = ?", [newPassword, token])
        g.db.commit()
        cursor.close()
        return True
    except:
        return False

#Check if password and token is correct
def check_password(token, oldPassword):
    try:
        salt = get_salt_by_token(token)
        oldPassword = hashpw(str(oldPassword), str(salt))
        cursor = g.db.execute("select password from user where token = ?", [token])
        password = cursor.fetchone()[0]
        cursor.close()
        if password == oldPassword:
            return True
        return False
    except:
        return False

#Retrieve user data with token
def get_user_data_token(token):
    try:
        result = []
        cursor = g.db.execute("select email, firstname, familyname, gender, city, country from user where token = ?", [token])
        rows = cursor.fetchall()
        cursor.close()
        result.append({'email':rows[0][0], 'firstname':rows[0][1], 'familyname':rows[0][2], 'gender':rows[0][3], 'city':rows[0][4], 'country':rows[0][5]})
        if len(result) > 0:
            return jsonify(result)
        return False
    except:
        return False

#Retrieve user data with email and token
def get_user_data_email(email):
    try:
        result = []
        cursor = g.db.execute("select email, firstname, familyname, gender, city, country from user where email = ?", [email])
        rows = cursor.fetchall()
        cursor.close()
        result.append({'email':rows[0][0], 'firstname':rows[0][1], 'familyname':rows[0][2], 'gender':rows[0][3], 'city':rows[0][4], 'country':rows[0][5]})
        if len(result) > 0:
            return jsonify(result)
        return False
    except:
        return False

#Create post for a user with retrieved email
def create_post(sender, receiver, message):
    try:
        cursor = g.db.execute("insert into message values(?,?,?)", [receiver, sender, message])
        g.db.commit()
        cursor.close()
        return True
    except:
        return False

#Get messages for specific user email
def get_user_message_email(email):
    try:
        result = []
        cursor = g.db.execute("select content,sender from message where receiver = ?", [email])
        rows = cursor.fetchall()
        cursor.close()
        for index in range(len(rows)):
            result.append({'content':rows[index][0],'sender': rows[index][1]})
        if len(result) > 0:
            return jsonify(result)
        return False
    except:
        return False


def get_salt_by_email(email):
    try:
        cursor = g.db.execute("select salt from user where email = ?", [email])
        salt = cursor.fetchone()[0]
        cursor.close()
        return salt
    except:
        return False

def get_salt_by_token(token):
    try:
        print(token)
        cursor = g.db.execute("select salt from user where token = ?", [token])
        salt = cursor.fetchone()[0]
        cursor.close()
        return salt
    except:
        return False
