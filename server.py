__author__ = 'Hugo Moritz and Jonatan Barr'
from flask import app, request
from flask import Flask
from flask import jsonify
import uuid
import database_helper
import json
from flask import g
import sqlite3
import urllib
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from gevent import pywsgi
from geventwebsocket import WebSocketError
import hashlib

app = Flask(__name__)
app.debug = True
#bcrypt = Bcrypt(app)

socketlist = dict()

@app.teardown_request
def teardown_request(exception):
    database_helper.close_db();

@app.before_request
def before_request():
    database_helper.connect_db();

@app.route('/', methods=['GET'])
def root():
    return app.send_static_file('client.html')

@app.route('/home', methods=['GET'])
def home():
    return app.send_static_file('client.html')

@app.route('/browse', methods=['GET'])
def browse():
    return app.send_static_file('client.html')

@app.route('/account', methods=['GET'])
def account():
    return app.send_static_file('client.html')


#sign-in fukar och returnerar token vid korrekt inloggning
@app.route('/sign-in',methods=['POST'])
def sign_in():
    email = request.form["email"]
    password = request.form["password"]
    if email != None and password != None:
        result = []
        result = database_helper.email_password(email, password)
        if result == True:
            token = str(uuid.uuid4())
            token_result = database_helper.save_token(email, password, token)
            if token_result == True:
                if email in socketlist:
                    try:
                        websocket = socketlist[email]
                        websocket.send(json.dumps({'success' : False}))
                    except WebSocketError as e:
                        del socketlist[email]
                return jsonify(token=token), 200
            else:
                return "Could not save token", 400
        else:
            return 'Wrong username or password', 400
    else:
        return "Missing email or password", 400

#Register a user to the database. Sends information to database_helper if correct.
@app.route('/register',methods=['POST'])
def sign_up():
    email = request.form["email"]
    password = request.form["password"]
    firstname = request.form["firstname"]
    familyname = request.form["familyname"]
    gender = request.form["gender"]
    city = request.form["city"]
    country = request.form["country"]
    c_password = 8
    if email != None and password != None and firstname != None and familyname != None and gender != None and city != None and country != None:
        if len(password) < c_password:
            return 'Password have to be at least '+ str(c_password) + ' characters', 404
        result = database_helper.register(email, password, firstname, familyname, gender, city, country)
        if result == False:
            return "User registration failed", 400
        return "User registration complete", 200
    else:
        return "Missing information", 400

def get_token (hashedData, path, email):
    token = database_helper.get_token_by_email(email)
    hash_str = path + email + token
    checksum = hashlib.sha256(hash_str.encode('ascii')).hexdigest()
    result = database_helper.get_user_data_token(token)
    if hashedData == checksum:
        return token
    else:
        return False;

#Signs out a user.
@app.route('/sign-out',methods=['POST'])
def sign_out():
    token = get_token(request.form["hashedData"],'/sign-out', request.form["email"])
    if token != False:
        email = database_helper.get_email(token)
        result = database_helper.delete_token(email)
        if result == True:
            return "Signed out", 200
        else:
            return "Failed to sign out", 400
    else:
        return "Failed token, could not sign out", 401

#Changes password
@app.route('/change-password',methods=['POST'])
def change_pass():
    auth = get_token(request.form["hashedData"],'/change-password', request.form["email"])
    oldPassword = request.form["oldPassword"]
    newPassword = request.form["newPassword"]
    check = database_helper.check_password(auth, oldPassword)
    c_password = 8
    if auth != False and len(newPassword) >= c_password and check == True:
        result = database_helper.set_password(auth, newPassword)
        if result == True:
            return "Password changed", 200
        else:
            return "Password could not change", 400
    else:
        return "Password is incorrect or wrong length", 400

#Get user data with token
@app.route('/user', methods=['POST'])
def get_user_data_by_token():
    token = get_token(request.form["hashedData"], '/user', request.form["email"])
    result = database_helper.get_user_data_token(token)
    if result != False and token != False:
        return result, 200
    else:
        return "", 401

#Get user data from email, and verufy with current users token
@app.route('/user2', methods=['POST'])
def get_user_data_by_email():
    token = get_token(request.form["hashedData"],'/user2', request.form["email"])
    email = request.form["useremail"]
    if token != False and email != None:
        result_email = database_helper.get_user_data_email(email)
        if result_email != False:
            return result_email, 200
        else:
            return "", 400
    else:
        return "", 401

#Create post on a users wall
@app.route('/post-message',methods=['POST'])
def post():
    token = get_token(request.form["hashedData"],'/post-message', request.form["email"])
    sender = request.form["email"]
    receiver = request.form["receiveremail"]
    message = request.form["message"]
    if token != False:
        sender = database_helper.get_email(token)
        result_post = database_helper.create_post(sender,receiver, message)
        if result_post == True:
            return "Post created", 200
        else:
            return "Could not post message", 400
    else:
        return "Could not post message, incorrect token", 401

#Get user message by email
@app.route('/message', methods=['POST'])
def get_user_message_by_email():
    token = get_token(request.form["hashedData"], '/message',request.form["email"])
    email = request.form["useremail"]
    print("debug1")
    if token != False and email != None:
        print("debug2")
        result = database_helper.get_user_message_email(email)
        print("debug3")
        if result != False:
            print("debug4")
            return result, 200
        else:
            print("debug5")
            return "Could not see messages", 400
    else:
        return "Could not access messages", 403


#websocket
@app.route('/api')
def api():
    if request.environ.get('wsgi.websocket'):
        websocket = request.environ['wsgi.websocket']
        obj = websocket.receive()
        data = json.loads(obj)
        token = database_helper.get_token(data['token'])
        if token == False:
            websocket.send(json.dumps({"message": "Error. You must be signed in first."}))
            return ''
        try:
            email = database_helper.get_email(data['token'])
            socketlist[email] = websocket
            while True:
                obj = websocket.receive()
                if obj == None:
                    if email in socketlist.keys():
                        del socketlist[email]
                    websocket.close()
        except WebSocketError as e:
            if email in socketlist.keys():
                del socketlist[email]
    return ''


if __name__ == '__main__':
    server = WSGIServer(('', 5000), app, handler_class=WebSocketHandler )
    server.serve_forever()
