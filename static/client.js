var thisUsersDataHasBeenSet= false;

// not needed ...
function index() {}
function home() {}
function browse() {}
function account() {}


function displayLoginView() {
		resetFields();
		document.getElementById("home_view").style.display = "block";
		document.getElementById("browse_view").style.display = "none";
		document.getElementById("account_view").style.display = "none";
		document.getElementById("home").style.textDecoration = "underline";
		document.getElementById("browse").style.textDecoration = "none";
		document.getElementById("account").style.textDecoration = "none";
		document.getElementById("profileview").style.display="none";
		document.getElementById("loginview").style.display="block";
}

function displayBrowsePage () {
		resetFields();
		document.getElementById("home_view").style.display = "none";
		document.getElementById("browse_view").style.display = "block";
		document.getElementById("account_view").style.display = "none";
		document.getElementById("home").style.textDecoration = "none";
		document.getElementById("browse").style.textDecoration = "underline";
		document.getElementById("account").style.textDecoration = "none";
		document.getElementById("profileview").style.display="block";
		document.getElementById("loginview").style.display="none";
		page('/browse');
		history.pushState({page: "browse"}, "page 2", 'browse');
}


function displayAccountPage () {
		resetFields();
		document.getElementById("home_view").style.display = "none";
		document.getElementById("browse_view").style.display = "none";
		document.getElementById("account_view").style.display = "block";
		document.getElementById("home").style.textDecoration = "none";
		document.getElementById("browse").style.textDecoration = "none";
		document.getElementById("account").style.textDecoration = "underline";
		document.getElementById("profileview").style.display="block";
		document.getElementById("loginview").style.display="none";
		page('/account');
		history.pushState({page: "account"}, "page 3", 'account');
}


function displayHomePage() {
		resetFields();
		document.getElementById("home_view").style.display = "block";
		document.getElementById("browse_view").style.display = "none";
		document.getElementById("account_view").style.display = "none";
		document.getElementById("home").style.textDecoration = "underline";
		document.getElementById("browse").style.textDecoration = "none";
		document.getElementById("account").style.textDecoration = "none";
		document.getElementById("profileview").style.display="block";
		document.getElementById("loginview").style.display="none";
		setUserData();
		page('/home');
		history.pushState({page: "home"}, "page 1", 'home');
}


function displayContentFromUrl () {
	displayView();
	var token = localStorage.getItem("token");
	if(token !== null) {
		var file, n;
		file = window.location.pathname;
		n = file.lastIndexOf('/');
		if (n >= 0) {
			file = file.substring(n + 1);
		}
		if(file == "home") {
			//console.log("currentpage home");
			displayHomePage();
		} else if(file == "browse") {
			//console.log("currentpage browse");
			displayBrowsePage();
		} else if(file == "account") {
			//console.log("currentpage account");
			displayAccountPage();
		}
	 }
}

window.addEventListener('popstate', function (event) {
/*
window.history.back();
var currentState = history.state;
//console.log("popstate" + currentState.page);

location.reload();
*/
});

window.onload = function(e){
	displayContentFromUrl();
}


function sendPostRequest(path, data, callbackFunction) {

	var http = new XMLHttpRequest();
	http.open("POST", path, true);
	var returnmessage="";
	var formdata = "";
	var datapairs = [];
	var name;
	for(name in data) {
		datapairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
	}
	formdata = datapairs.join('&').replace(/%20/g, '+');
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.onreadystatechange = function() {
		if(http.readyState == 4) {
			callbackFunction(http.status, http.responseText);
		}
	}
	http.send(formdata);

	return http.responseText;
}

function sendSecurePostRequest(path, data, callbackFunction) {
	var http = new XMLHttpRequest();
	http.open("POST", path, true);
	var returnmessage="";
	var formdata = "";
	var datapairs = [];
	var name;
	var emailParameterExists = false;
	for(name in data) {
			datapairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
		if(name == "email") emailParameterExists = true;
	}
	if(!emailParameterExists) {
		datapairs.push("email=" + encodeURIComponent(localStorage.getItem("email")));
	}
	datapairs.push("hashedData=" + sha256(path + localStorage.getItem("email") + localStorage.getItem("token")));
	formdata = datapairs.join('&').replace(/%20/g, '+');
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.onreadystatechange = function() {
		if(http.readyState == 4) {
			callbackFunction(http.status, http.responseText);
		}
	}
	http.send(formdata);
	return http.responseText;
}



	function signUp() {

		var formData = {
			email: document.forms["signupform"]["email"].value,
			password: document.forms["signupform"]["password"].value,
			firstname: document.forms["signupform"]["firstname"].value,
			familyname: document.forms["signupform"]["familyname"].value,
			city: document.forms["signupform"]["city"].value,
			gender: document.forms["signupform"]["gender"].value,
			country: document.forms["signupform"]["country"].value
		};

		var callbackFunction = (function(httpcode,msg) {
			sendMessageToUser(msg);
    });

		sendPostRequest("/register", formData, callbackFunction);

	}



	function signIn() {
		var email = document.forms["signinform"]["email"].value;
		var formData = {
			email: email,
			password: document.forms["signinform"]["password"].value
		};

		var callbackFunction = (function(httpcode,msg) {
				if(httpcode == 200) {
					thisUsersDataHasBeenSet = false;
					var token = JSON.parse(msg)["token"];
					localStorage.removeItem("token");
					localStorage.removeItem("email");
					localStorage.setItem("token", token);
					localStorage.setItem("email", email);
					//console.log("email: " + email);
					sendMessageToUser("You have signed in.");
					displayView();
					displayHomePage();
					socket();
				} else {
					sendMessageToUser(msg);
				}
		});
		sendPostRequest("/sign-in", formData, callbackFunction);

	}


	function resetFields() {
		//console.log("reseting");
		document.getElementById("msg").innerHTML = "";
		var inputs, index;
		inputs = document.getElementsByTagName('input');
		for (index = 0; index < inputs.length; ++index) {
			if(inputs[index].getAttribute("type") !== "submit" )
				inputs[index].value= "";
		}
	}



	function sendMessageToUser(msg) {
		document.getElementById("msg").innerHTML = msg;
	}


	function setUserData(email="") {
		var userinfodiv;
		var callbackFunction = (function(httpcode,msg) {
			if(httpcode == 200) {
					if(email == "") {
						userinfodiv = document.getElementById("thisuserinfobox");
						thisUsersDataHasBeenSet = true;
					} else {
						userinfodiv = document.getElementById("otheruserinfobox");
						document.getElementById("otheruserinfobox").style.display = "block";
					}
					var userdata = JSON.parse(msg)[0];
					var useremail = userdata["email"];
					var wall = userinfodiv.getElementsByClassName("wall")[0];
					wall.setAttribute('data-user', useremail);
					refreshWall(wall);
					userinfodiv.getElementsByClassName("firstname")[0].innerHTML = userdata["firstname"];
					userinfodiv.getElementsByClassName("email")[0].innerHTML = userdata["email"];
					userinfodiv.getElementsByClassName("familyname")[0].innerHTML = userdata["familyname"];
					userinfodiv.getElementsByClassName("gender")[0].innerHTML = userdata["gender"];
					userinfodiv.getElementsByClassName("city")[0].innerHTML = userdata["city"];
					userinfodiv.getElementsByClassName("country")[0].innerHTML = userdata["country"];
					userinfodiv.getElementsByClassName('send_msg')[0].onclick = function() {

							var callbackFunction = (function(httpcode,msg) {
								sendMessageToUser(msg);
							});

							var formData = {
								//Authorization: localStorage.getItem("token"),
								receiveremail: useremail,
								message: userinfodiv.getElementsByClassName("message")[0].value
							};
							sendSecurePostRequest("/post-message", formData, callbackFunction);
				}
			} else {
				sendMessageToUser(msg);
			}
		});
		var token = localStorage.getItem("token");

		if(email == "") {
			if(!thisUsersDataHasBeenSet) {
				sendSecurePostRequest("/user", {}, callbackFunction);
			}
		} else {
				sendSecurePostRequest("/user2", {useremail: email}, callbackFunction);
		}
	}





	function signOut() {
			resetFields();
			displayLoginView();

			var callbackFunction = function (httpcode, msg) {
				sendMessageToUser(msg);
			}
			sendSecurePostRequest("/sign-out", {}, callbackFunction);
			page('/');
	}


	function changePassword(oldpass, newpass1, newpass2) {
		if(newpass1 == newpass2) {
			var token = localStorage.getItem("token");
			var callbackFunction = (function(httpcode,msg) {
					  sendMessageToUser(msg);
			});
			var formData = {
				newPassword: newpass1,
				oldPassword: oldpass
			};
			sendSecurePostRequest("/change-password", formData, callbackFunction);
		} else {
			sendMessageToUser("Passwords does not match.");
		}
	}

	function refreshWall(wall) {
		resetFields();
		//console.log("refreshing...");
		var token =localStorage.getItem("token");
		var useremail = wall.getAttribute('data-user');
		wall.innerHTML  ="";
		var callbackFunction = (function(httpcode,msg) {
			if(httpcode == 200) {
				msg = JSON.parse(msg);
				//console.log(msg);
			for(var i=0; i< msg.length; i++) {
		   		wall.innerHTML += "<div class='messageHolder' id='message"+i+"' draggable='true' ondragstart='drag(event)' > From:" + msg[i]["sender"] + "<br/><div class='messagetext'>" + msg[i]["content"]+ "</div></div>";
			}
			}
		});
		var formData = {
	 		useremail: useremail
		};
		sendSecurePostRequest("/message", formData, callbackFunction);
	}



	function allowDrop(ev) {
 	   ev.preventDefault();
	}

	function drag(ev) {
		ev.dataTransfer.setData("text", ev.target.id);
	}

	function drop(ev) {
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		var messagediv = document.getElementById(data);
		var messagetext = messagediv.getElementsByClassName("messagetext")[0].textContent;
		//console.log(messagetext);
		var containers = document.getElementsByClassName("message");
		containers[0].value = messagetext;
		containers[1].value = messagetext;
	}


	function displayView () {
		var token = localStorage.getItem("token");

		document.getElementById('home').onclick = function() {
			  displayHomePage();
		};

		document.getElementById('browse').onclick = function() {
		     displayBrowsePage();
		};

		document.getElementById('account').onclick = function() {
		     displayAccountPage();
		};

		document.getElementById('display_user_wall').onclick = function() {
			var user = document.getElementById("useremail").value;
			setUserData(user);
		};

		document.getElementById('logout').onclick = function() {
			signOut();
			localStorage.removeItem('token');
			localStorage.removeItem('email');
		};

		document.getElementById('changepassword').onclick = function() {
				changePassword(
					document.forms["passwordform"]["oldpassword"].value,
					document.forms["passwordform"]["newpassword1"].value,
					document.forms["passwordform"]["newpassword2"].value
				);
		};



	}



//-----------------------------------------------

socket = function(){
	websocket = new WebSocket("ws://" + document.domain +  ":5000/api");

	websocket.onopen = function() {

		websocket.send(JSON.stringify({"token":localStorage.getItem("token")}));
	};

	websocket.onmessage = function(msg) {
		if (JSON.parse(msg.data)["success"] == false) {
			displayLoginView();
			localStorage.removeItem('token');

		}
	};

}
