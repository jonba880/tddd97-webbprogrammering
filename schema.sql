drop table IF EXISTS user;
drop table IF EXISTS message;
create table user(email text, password text, firstname text, familyname text, gender text, city text, country text, token text, salt text, primary key(email));
create table message(receiver text, sender text, content text);

insert into user values("h@h.com", "hhh", "hugo", "king", "m", "lkpg", "swe", NULL, NULL);
insert into user values("g@g.com", "ggg", "carl", "magnus", "m", "lkpg", "swe", NULL, NULL);

insert into message values("y@y.com", "y@y.com", "Hey whats up");
insert into message values("y@y.com", "y@y.com", "Hey whats up1");
insert into message values("y@y.com", "y@y.com", "Hey whats up2");
insert into message values("h@h.com",  "y@y.com","Hey whats up3");
